﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    float inputHorizontal;
    float horizontalVelocity = 0;
    public float speedMultiplier = 1.33f;
    string direction; // l r n left right neutral
    string lastDirection = "n"; // direction from last frame

    float acceleration = 1f;
    public float accelRate = 1.08f;
    public float maxVelocity = 13.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        KeyboardMovement();

    }

    private void KeyboardMovement()
    {
        // get axis and rigidbody
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        inputHorizontal = Input.GetAxisRaw("Horizontal");
        
        // get direction
        if (inputHorizontal == 0)
            direction = "n";
        else if (inputHorizontal > 0)
            direction = "r";
        else direction = "l";

        // determine acceleration
        if(direction == lastDirection && direction != "n")
        {
            acceleration = acceleration * accelRate;
        }
        if (direction != lastDirection)
            acceleration = 1f;

        // apply multiplier and accel
        horizontalVelocity = inputHorizontal * speedMultiplier * acceleration;

        
        // check min max
        if (horizontalVelocity > maxVelocity)
            horizontalVelocity = maxVelocity;
        else if (horizontalVelocity < maxVelocity * -1)
            horizontalVelocity = maxVelocity * -1;

       

        // set velocity
        rb.velocity = new Vector2(horizontalVelocity, 0.0f);


        // set last direction
        lastDirection = direction;

        

    }
}
